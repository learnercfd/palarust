use std::env;
use std::io;
use std::io::prelude::*;
use std::process::exit;

mod cylinder;
mod poiseuille;

extern crate palarust;
extern crate num_cpus;
extern crate docopt;
extern crate rustc_serialize;

const USAGE: &'static str = "
	Usage: palarust-demo <demo-name> [ options ]
	       palarust-demo --help
	A collection of different examples of Palarust. 
	You can run the individual examples by running
	`palarust-demo foo`, where `foo` is the name of a example. Each
	example has its own options and modes, so try `palarust-demo foo
	--help`.
	Benchmarks:
	  - cylinder:   The parallel simulation of the flow past a ciricular cylinder.
	  - poiseuille: The parallel simulation of the poiseuille flow.
	";

fn usage() -> ! {
    let _ = writeln!(&mut io::stderr(), "{}", USAGE);
    exit(1);
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        usage();
    }

    let bench_name = &args[1];
    match &bench_name[..] {
        "cylinder"   => cylinder::main(&args[1..]),
        "poiseuille" => poiseuille::main(&args[1..]),
        // "cylinder" => cylinder::main(&args[1..]),
		_ => usage()
    }
}