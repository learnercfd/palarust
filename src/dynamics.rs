use cell;
use moments::Moments;

/// Parameters trait containing interfaec to the standard parameters of
/// Cell (relaxation frequency and other things...).
pub trait Parameters{
    fn get_rel_freq<'a>(&'a self) -> &'a f64;

    fn set_rel_freq(&mut self, omega: f64);
}

/// Dynamics trait which regroup the basic functions
/// that are making the dynamics of the LBM.
pub trait Dynamics : Moments+Parameters
{
	/// Computation of the equilibria functions from rho and u
	fn compute_equilibria(&self, rho: &f64, u: &[f64; cell::D]) -> [f64; cell::Q];

	/// Computation of the non-equilibria from pi (2nd order Hermite coefficient)
	/// of the non-equilibrium distribution function
	fn compute_non_equilibria(&self, pi: &[[f64; cell::D]; cell::D]) -> [f64; cell::Q];

	/// decomposes the population into rho, u, pi, (which correspond to the order 1 Chapman-Enskog decomposition, freg=feq(rho,u)+fone(pi))
	/// and fneq = f-freg. One thus has f = feq+fone+fneq.
	fn decompose(&self, f: &[f64; cell::Q]) -> (f64, [f64; cell::D], [[f64; cell::D]; cell::D], [f64; cell::Q]) {
		let rho = self.compute_rho(f);
		let u = self.compute_u(f);
		let feq = self.compute_equilibria(&rho, &u);
		let pi = self.compute_deviatoric_stress(f);
		let fone = self.compute_non_equilibria(&pi);

		let fneq = f.iter().zip(feq.iter().zip(fone.iter())).map(|(&f_i, (&feq_i, &fone_i))| f_i-(feq_i+fone_i)).collect::<Vec<f64>>();
		(rho, u, pi, *array_ref!(fneq[0..cell::Q], 0, cell::Q))
	}

	/// recomposes the population from rho, u, pi, (which correspond to the order 1 Chapman-Enskog decomposition, freg=feq(rho,u)+fone(pi))
	/// and fneq = f-freg. One thus has f = feq+fone+fneq.
	fn recompose(&self, f: &mut [f64; cell::Q], rho: &f64, u: &[f64; cell::D], pi: &[[f64; cell::D]; cell::D], fneq: &[f64; cell::Q]) {
		let feq = self.compute_equilibria(&rho, &u);
		let fone = self.compute_non_equilibria(&pi);
		let f_tot = fneq.iter().zip(feq.iter().zip(fone.iter())).map(|(&fneq_i, (&feq_i, &fone_i))| fneq_i+feq_i+fone_i).collect::<Vec<f64>>();
		for i in 0..cell::Q {
			f[i] = f_tot[i];
		}
	}

	/// Regularization of the distribution functions
	fn regularize(&self, f: &mut [f64; cell::Q], rho: &f64, u: &[f64; cell::D], pi: &[[f64; cell::D]; cell::D]) {
		let feq = self.compute_equilibria(rho, u);
		let fone = self.compute_non_equilibria(pi);
		for i in 0..cell::Q {
			f[i] = feq[i] + fone[i];
		}
	}

	/// Sets the density of the population. By default it uses decompose/recompose functions.
	fn set_rho(&mut self, f: &mut [f64; cell::Q], rho_new: &f64) {
		let (_rho, u, pi, fneq) = self.decompose(f);
		self.recompose(f, &rho_new, &u, &pi, &fneq);
	}

	/// Sets the velocity of the population. By default it uses decompose/recompose functions.
	fn set_u(&mut self, f: &mut [f64; cell::Q], u_new: &[f64; cell::D]) {
		let (rho, _u, pi, fneq) = self.decompose(f);
		self.recompose(f, &rho, &u_new, &pi, &fneq);
	}

	/// Sets the deviatoric stress of the population. By default it uses decompose/recompose functions.
	fn set_deviatoric_stress(&mut self, f: &mut [f64; cell::Q], pi_new: &[[f64; cell::D]; cell::D]) {
		let (rho, u, _pi, fneq) = self.decompose(f);
		self.recompose(f, &rho, &u, &pi_new, &fneq);
	}

	/// Collision operator
	fn collide(&self, f: &mut [f64; cell::Q]);

	/// Swap of oposite distribution functions (used for streaming)
	fn swap(&self, f: &mut [f64; cell::Q]) {
		let half = (f.len()-1)/2;
		for i in 1..(half+1) {
			f.swap(i,i+half);
		}
	}

	/// Collides and then swaps distribution functions
	fn collide_and_swap(&self, f: &mut [f64; cell::Q]) {
		self.collide(f);
		self.swap(f);
	}


}

