use std::error::Error;
use std::fs::File;
use std::path::Path;
use cell;
use sdl2;
use sdl2::render::Renderer;
use sdl2::pixels::Color;
use colormaps;

/// Creates a String given a preffix, an integer , and a suffix
///
/// # Arguments
///
/// * `preffix` - str contining the beginning of the new name
/// * `i` - integer which will be converted to string and put inbetween preffix and suffix
/// * `suffix` - str contining the end of the new name
///
/// # Example
///
/// ```
/// extern crate palarust;
/// use palarust::io;
/// fn main () {
///     assert_eq!(io::create_name("preffix", 0, "suffix"), "preffix0suffix" );
/// }
/// ```
pub fn create_name(preffix: &'static str, i: i32, suffix: &'static str) -> String {
	String::new()+ preffix + &i.to_string() +suffix
}

/// Initialization of a white-filled window that is shown on the screen
/// # Arguments
///
/// * `nx` - size of the window in the x-direction.
/// * `nx` - size of the window in the y-direction.
/// # Example
///
/// ```rust,no_run
/// extern crate palarust;
/// use palarust::io;
/// fn main () {
///     io::init(200,100);
/// }
/// 
/// ```
pub fn init<'a>(nx: u32, ny: u32)-> Renderer<'a> {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem.window("demo", nx, ny)
        .position_centered()
        .opengl()
        .build()
        .unwrap();

    let mut renderer = window.renderer().build().unwrap();

    renderer.set_draw_color(Color::RGB(255, 255, 255));
    renderer.clear();
    renderer.present();

    renderer
}

pub trait IO {
	// fn create(&self, fname: &'static str) -> File {
	fn create(&self, fname: String) -> File {
		let path = Path::new(&fname);
	    let display = path.display();

	    // Open a file in write-only mode, returns `io::Result<File>`
	    let file = match File::create(&path) {
	        Err(why) => panic!("couldn't create {}: {}",
	                           display,
	                           why.description()),
	        Ok(file) => file,
	    };
	    file
	}

	fn write_scalar<Fun>(&self, file: &mut File, foo: Fun) where Fun: Fn(&[f64; cell::Q]) -> f64;

	fn write_scalar_to_png<Fun>(&self, fname: String, foo: Fun) where Fun: Fn(&[f64; cell::Q]) -> f64;

	#[allow(unused_must_use)]
	fn show_scalar<Fun>(&self, r: &mut Renderer, foo: Fun, cm: &colormaps::ColorMap) 
		where Fun: Fn(&[f64; cell::Q]) -> f64;
}


