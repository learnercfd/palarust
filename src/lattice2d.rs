use core::clone::Clone;
use cell;
use cell::{Cell, MutCell};
use lattice_ops_2d::LatticeOps2d;
use std::mem;
use dynamics::Dynamics;
use std::f64;
use point2d::Point2d;
use box2d::Box2d;
use ind_hlp;

/// Builder for the Lattice2d struct
pub struct Lattice2dBuilder{
	pops: Vec<f64>,
	nx: usize,
	ny: usize,
	dyn: Vec<Box<Dynamics>>,
	loc: Point2d,
}

impl Lattice2dBuilder {
	pub fn new(nx: usize, ny: usize, loc: Point2d) -> Self {
		Lattice2dBuilder{ pops: Vec::with_capacity(nx*ny*cell::Q*mem::size_of::<f64>()), 
			nx: nx, ny: ny, dyn: Vec::with_capacity(nx*ny*mem::size_of::<Box<Dynamics>>()), 
			loc: loc }
	}

	pub fn set_dyn<T: Dynamics+Clone>(mut self, c: T) -> Self {
		for _ in 0..self.nx*self.ny {
			let feq = c.compute_equilibria(&1.0, &[0.0, 0.0]);
			for f in feq.iter() {
				self.pops.push(*f);
			}
			self.dyn.push(Box::new(c.clone()));
		}
		self
	}

	pub fn finalize(self) -> Lattice2d {
		Lattice2d{pops: self.pops, nx: self.nx, ny: self.ny, dyn: self.dyn, loc: self.loc,}
	}
}

/// The Lattice2d struct contains the populations and dynamics of the 
/// lattice run on each thread.
#[derive(Debug)]
pub struct Lattice2d {
	/// Vector containing the populations
	pops: Vec<f64>,
	/// X-size of the lattice
	nx: usize,
	/// Y-size of the lattice
	ny: usize,
	/// Vector containing the dynamics
	dyn: Vec<Box<Dynamics>>,
	/// Offset of the position for translating into absolute coordinates
	loc: Point2d,
}

unsafe impl Send for Lattice2d {}
unsafe impl Sync for Lattice2d {}


impl Lattice2d {
	/// Sets a dynamics on a lattice according to a position related predicate
	pub fn set_gen_dyn<V, Fmask>(&mut self, c: V, foo: Fmask)  
		where V: Dynamics+Clone, Fmask : Fn(i32,i32) -> bool
	{
		for i in 0..self.dyn.len() {
			let (x,y) = ind_hlp::get_abs_grid_indices(i,self.nx,self.ny,self.loc);
			if foo(x,y) {
				self.dyn[i] = Box::new(c.clone());
			}
		}
	}

	/// Sets a dynamics on the i-th position
	pub fn set_dyn<T: Dynamics+Clone>(&mut self, i:usize, c: T) {
		assert!(i < self.nx*self.ny, "{:?} index must be smaller than {:?} to be in bounds.", i, self.nx*self.ny);
		self.dyn[i] = Box::new(c);
	}

	pub fn get_pops<'a>(&'a self, i: usize) -> &'a [f64; cell::Q] {
		array_ref!(self.pops[i*cell::Q..(i+1)*cell::Q], 0, cell::Q)
	}

	pub fn get_mut_cell(&mut self, i: usize) -> MutCell {
		MutCell{dyn: &mut (*self.dyn[i]), pops: array_mut_ref!(self.pops[i*cell::Q..(i+1)*cell::Q], 0, cell::Q)}
	}

	pub fn get_cell(&self, ix: usize, iy: usize) -> Cell {
		let i = ind_hlp::get_grid_idx(ix, iy, self.get_nx(), self.get_ny());
		Cell{dyn: &(*self.dyn[i]), pops: array_ref!(self.pops[i*cell::Q..(i+1)*cell::Q], 0, cell::Q)}
	}

	pub fn get_pops_from_box(&self, b: &Box2d) -> Vec<f64> {
		let mut pops_tmp = Vec::with_capacity(b.size()*cell::Q*mem::size_of::<f64>());
		for x in b.x0..b.x1 {
			for y in b.y0..b.y1 {
				let j = ind_hlp::get_raw_grid_idx(x as usize,y as usize,cell::Q, self.nx, self.ny);
				for i in 0..cell::Q {
					pops_tmp.push(self.pops[j+i]);
				}
			}
		}
		pops_tmp
	}

	pub fn copy_pops(&mut self, to: &Vec<Box2d>, next_pops: &Vec<Vec<f64>>) {
		assert_eq!(next_pops.len(), to.len());
		for (i,b) in to.iter().enumerate() {
			let pop = &next_pops[i];
			assert_eq!(pop.len(), b.size()*cell::Q);
			let mut j = 0;
			for x_to in b.x0..b.x1 {
				for y_to in b.y0..b.y1 {
					let i_to = ind_hlp::get_raw_grid_idx(x_to as usize, y_to as usize, cell::Q, self.nx, self.ny); 
					
					mem::replace(array_mut_ref!(self.pops[i_to..i_to+cell::Q], 0, cell::Q), 
					             *array_ref!(pop[j..j+cell::Q], 0, cell::Q) );
					j += cell::Q;
				}
			}
		}
	}

	pub fn get_nx(&self) -> usize {
		self.nx
	}
	pub fn get_ny(&self) -> usize {
		self.ny
	}

	pub fn get_loc<'a>(&'a self) -> &'a Point2d {
		&self.loc
	}
}

// ========== Implementation of LatticeOps2d trait for Lattice2d =========== //

impl LatticeOps2d for Lattice2d {
	fn collide(&mut self) {
		for i in 0..self.nx*self.ny {
			self.get_mut_cell(i).collide_and_swap();
		}
	}

	fn bulk_stream(&mut self) {
		for x in 1..self.nx-1 {
			for y in 1..self.ny-1 {
				// let mut c = self.pops[self.get_raw_index(x,y)];
    			let ind_one = ind_hlp::get_raw_grid_idx(x-1,y+1, cell::Q, self.nx, self.ny);
    			let ind_two = ind_hlp::get_raw_grid_idx(x-1,y  , cell::Q, self.nx, self.ny);
    			let ind_three = ind_hlp::get_raw_grid_idx(x-1,y-1, cell::Q, self.nx, self.ny);
    			let ind_four = ind_hlp::get_raw_grid_idx(x  ,y-1, cell::Q, self.nx, self.ny);
    			let ind_zero = ind_hlp::get_raw_grid_idx(x,y, cell::Q, self.nx, self.ny);

    			self.pops.swap(ind_one+1, ind_zero+5);
    			self.pops.swap(ind_two+2, ind_zero+6);
    			self.pops.swap(ind_three+3, ind_zero+7);
    			self.pops.swap(ind_four+4, ind_zero+8);
			}
		}
	}

	fn periodic_stream(&mut self) {
		// for x in 1..self.nx-1 {
		// 	for y in 1..self.ny-1 {
		for x in 0..self.nx {
			for y in 0..self.ny {
				let x_m1 = ((x as i32-1+self.nx as i32) % self.nx as i32) as usize;
				// let x_p1 = ((x as i32 +1+self.nx as i32) % self.nx as i32) as usize;

				let y_m1 = ((y as i32-1+self.ny as i32) % self.ny as i32) as usize;
				let y_p1 = ((y as i32+1+self.ny as i32) % self.ny as i32) as usize;


				let ind_zero = ind_hlp::get_raw_grid_idx(x,y, cell::Q, self.nx, self.ny);
    			let ind_one = ind_hlp::get_raw_grid_idx(x_m1,y_p1, cell::Q, self.nx, self.ny);
    			let ind_two = ind_hlp::get_raw_grid_idx(x_m1,y  , cell::Q, self.nx, self.ny);
    			let ind_three = ind_hlp::get_raw_grid_idx(x_m1,y_m1, cell::Q, self.nx, self.ny);
    			let ind_four = ind_hlp::get_raw_grid_idx(x  ,y_m1, cell::Q, self.nx, self.ny);

    			self.pops.swap(ind_one+1, ind_zero+5);
    			self.pops.swap(ind_two+2, ind_zero+6);
    			self.pops.swap(ind_three+3, ind_zero+7);
    			self.pops.swap(ind_four+4, ind_zero+8);
			}
		}
	}

	fn bd_stream(&mut self) {
		for x in 0..self.nx {
			let step = if x == 0 || x==self.nx-1 { 1 } else { self.ny-1 };
			for y in (0..self.ny).step_by(step) {
				let ind_two = ind_hlp::get_raw_grid_idx(x,y,cell::Q,self.nx,self.ny);
                for i in 1..(cell::Q/2+1) {
			        let mut xx = x as i32 +cell::C[i][0];
			        let mut yy = y as i32 +cell::C[i][1];
			        if xx<0 {xx=self.nx as i32 -1}; 
			        if xx>=self.nx as i32  {xx=0};
			        if yy<0 {yy=self.ny as i32 -1}; 
			        if yy>=self.ny as i32  {yy=0};
    				let ind_one = ind_hlp::get_raw_grid_idx(xx as usize,yy as usize, cell::Q, self.nx, self.ny);
    				

					self.pops.swap(ind_one+i, ind_two+i+cell::Q/2);
				}
            }
        }
	}
}

