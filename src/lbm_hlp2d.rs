use cell;

/// Computes the symmetric part of a 2x2 matrix
/// 
/// # Arguments
///
/// * `a`  - 2x2 matrix
///
/// # Example
///
/// ```
/// extern crate palarust;
/// use palarust::lbm_hlp2d::symmetrize;
/// fn main () {
///     let a = [[1.0, 2.0], [3.0, 4.0]];
///		let b = symmetrize(&a);
///     
///     assert_eq!(b, [[1.0, 2.5], [2.5, 4.0]]);
///     assert_eq!(b, symmetrize(&b));
/// }
/// ```
pub fn symmetrize(a: &[[f64; cell::D]; cell::D]) -> [[f64; cell::D]; cell::D] {
	[ [ a[0][0]              ,  0.5*(a[0][1]+a[1][0]) ],
	  [ 0.5*(a[0][1]+a[1][0]),  a[1][1]]                ]

}

/// Computes the ant-isymmetric part of a 2x2 matrix
/// 
/// # Arguments
///
/// * `a`  - 2x2 matrix
///
/// # Example
///
/// ```
/// extern crate palarust;
/// use palarust::lbm_hlp2d::symmetrize;
/// use palarust::lbm_hlp2d::anti_symmetrize;
/// fn main () {
///     let a = [[1.0, 2.0], [3.0, 4.0]];
///		let b = symmetrize(&a);
///		let c = anti_symmetrize(&a);
///     let d = [[b[0][0] + c[0][0], b[0][1] + c[0][1]],
///				 [b[1][0] + c[1][0], b[1][1] + c[1][1]]];
///     
///     assert_eq!(c, [[0.0, -0.5], [0.5, 0.0]]);
///     assert_eq!(a, d);
/// }
/// ```
pub fn anti_symmetrize(a: &[[f64; cell::D]; cell::D]) -> [[f64; cell::D]; cell::D] {
	[ [ 0.0                  ,  0.5*(a[0][1]-a[1][0]) ],
	  [ 0.5*(a[1][0]-a[0][1]),  0.0]                ]

}

/// Computes the deviatoric stress from the strain rate.
/// 
/// # Arguments
///
/// * `strain`  - 2x2 matrix
/// * `rho`  - density
/// * `omega`  - relaxation frequency
///
/// # Example
///
/// ```
/// ```
pub fn strain_to_stress(strain: &[[f64; cell::D]; cell::D], rho: &f64, omega: &f64) -> [[f64; cell::D]; cell::D] {
	let s_to_pi = -2.0*rho*cell::CS2/omega;
	let mut pi = [[0.0; cell::D], [0.0; cell::D]];
	for ia in 0..cell::D {
		for ib in 0..cell::D {
			pi[ia][ib] = s_to_pi*strain[ia][ib];
		}
	}
	pi
}

